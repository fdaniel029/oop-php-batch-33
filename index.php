<?php

require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

// Sheep
$hewan = new Animal("shaun");
echo "Nama Hewan: " . $hewan->name . "<br>"; 
echo "Jumlah Kaki: " . $hewan->legs . "<br>"; 
echo "Berdarah Dingin: " . $hewan->cold_blooded . "<br><br>"; 

//Ape
$sungokong = new Ape("kera sakti");
echo "Nama Hewan: " . $sungokong->name . "<br>"; 
echo "Jumlah Kaki: " . $sungokong->legs . "<br>"; 
echo "Berdarah Dingin: " . $sungokong->cold_blooded . "<br>"; 
echo $sungokong->yell("Auooo") . "<br><br>"; 

//Frog
$kodok = new Frog("buduk");
echo "Nama Hewan: " . $kodok->name . "<br>"; 
echo "Jumlah Kaki: " . $kodok->legs . "<br>"; 
echo "Berdarah Dingin: " . $kodok->cold_blooded . "<br>"; 
echo $kodok->jump("hop hop") ; 
?>
